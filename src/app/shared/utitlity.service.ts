import { Injectable } from '@angular/core';

@Injectable({
    providedIn: "root"
})
export class UtilityService {
    comeFromDealFinder: boolean;

    isUserLogin: boolean = false;
}