import { CommonModalComponent } from './common-modal/common-modal.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LinkRendererComponent } from './components/renderers/link-renderer/link-renderer.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
  ],
  declarations: [
    CommonModalComponent,
    LinkRendererComponent,
  ],
  entryComponents: [
    CommonModalComponent,
    LinkRendererComponent,
  ],
  exports: [
    CommonModalComponent,
    LinkRendererComponent,
  ],
  providers: []
})
export class SharedModule { }
