import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, of, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ApplicationHttpClient } from './ApplicationHttpClient.service';

export interface Credentials {
  // Customize received credentials here
  username: string;
  access_token: string;
  interval_token: string;
  expires_in: number;
}

export interface LoginContext {
  username: string;
  password: string;
  group: string;
  remember?: boolean;
}

const credentialsKey = 'credentials';
const userRoleKey = 'userRole';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private _credentials: Credentials;
  private _userRole: any;
  isLogin:boolean;
  showSecurityTokenInput: boolean = false;

  loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);


  constructor(private router: Router, private http: HttpClient, private httpc: ApplicationHttpClient) {
    this._credentials = JSON.parse(sessionStorage.getItem(credentialsKey) || localStorage.getItem(credentialsKey));
    this._userRole = sessionStorage.getItem(userRoleKey);
  }

  login(context: LoginContext) {

    const url = environment.serverUrl + '/oauth/token?grant_type=password&username=' + context.username + "&password=" + context.password;
    return this.http.post(url, {}).pipe(map(user => {
      // this.isLogin = true;
      
      const data = JSON.stringify(user);
      const userData = JSON.parse(data);
      userData.username = context.username;
      const credential = userData;
      this.setCredentials(credential, context.remember);

      return credential;
    }
    ));
  }

  logout() {
    // this.isLogin = false;
    // Customize credentials invalidation here
    // this.setCredentials();
    sessionStorage.clear();
    this.router.navigate(['pages/dashboard']);
    // return of(true);
    this.loggedIn.next(false);

  }

  isAuthenticated(): boolean {
    return !!this.credentials;
  }

  get credentials(): Credentials {
    return this._credentials;
  }

  private setCredentials(credentials?: any, remember?: boolean) {
    this._credentials = credentials || null;

    if (credentials) {
      const storage = remember ? localStorage : sessionStorage;
      storage.setItem(credentialsKey, JSON.stringify(credentials));
    } else {
      sessionStorage.removeItem(credentialsKey);
      localStorage.removeItem(credentialsKey);
      sessionStorage.removeItem(userRoleKey);
    }
  }

  loadUserRole(): Observable<any> {
    // const authHeader = new HttpHeaders()
    //   .set('Accept', 'application/json')
    //   .set('Content-Type', 'application/json')
    //   .set('Authorization', 'Bearer ' + this.credentials.access_token);
    const headerValues = new HttpHeaders({
      'Authorization': 'Bearer ' + this.credentials.access_token
    });
    const header = { headers: headerValues };

    const url: string = environment.serverUrl + 'user/by/';
    return this.http.get(url + this._credentials.username, header).pipe(map(res => {
      const data = JSON.stringify(res);
      const roleData = JSON.parse(data);
      console.log(roleData.role);
      sessionStorage.setItem(userRoleKey, roleData.role);
      return roleData.role;
    }));
  }
  setSecurityTokenInput(value:boolean){
    this.showSecurityTokenInput = value;
    }
  getUserRole(): any {
    return this._userRole;
  }
  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }
  getSignup(params): Observable<any> {
    return this.httpc.Post('v1/registration', params);
  }
  checkEmail(mail): any {
    return this.httpc.Get(`v1/checkemail/${mail}`);

  }
  checkUsername(user): any {
    return this.httpc.Get(`v1/checkusername/${user}`);

  }

}
