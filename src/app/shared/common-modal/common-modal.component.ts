import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbModalBackdrop} from '@ng-bootstrap/ng-bootstrap/modal/modal-backdrop';
@Component({
  selector: 'add-service-modal',
  styleUrls: ['./common-modal.component.scss'],
  templateUrl: './common-modal.component.html'
})

export class CommonModalComponent implements OnInit {

  modalHeader: string;

  showHide: boolean;

  modalContent: string;

  oKMessage: string = 'Subscribe';

  cancelMessage: string = 'Cancel';


  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit() { }


  cancelModal() {
    this.activeModal.close('N');
  }

  okModal() {
    this.activeModal.close('Y');
  }
}
