import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../../../router.animations';
@Component({
  selector: 'app-deals-details',
  templateUrl: './deals-details.component.html',
  styleUrls: ['./deals-details.component.scss']
})
export class DealsDetailsComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute) {
    //   console.log('changing page')
    //   let subuser = localStorage.getItem('user')

    //   if ( subuser != 'admin') {
    //     this.router.navigate(['pages/dashboard'])
    // }
  }

  ngOnInit() {

  }

  searchProInsights(){
    this.router.navigate(['pages/propertyInsightsModule/searchProperty'])
  }

}
