import {Component} from '@angular/core';

@Component({
  selector: 'propertyDeals',
  template: `<router-outlet></router-outlet>`
})
export class PropertyDeals {
  constructor() {
  }
}
