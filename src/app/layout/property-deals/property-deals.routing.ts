import { Routes, RouterModule } from '@angular/router';
import { PropertyDeals} from './property-deals.component';
import { DealsDetailsComponent } from './deals-details/deals-details.component';

const routes: Routes = [
  {
    path: '',
    component: PropertyDeals,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dealsDetails', component: DealsDetailsComponent },
    ]
  }
];

export const routing = RouterModule.forChild(routes);
