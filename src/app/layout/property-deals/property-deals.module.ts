import { CommonModule } from '@angular/common';
import { PropertyDeals} from './property-deals.component';
import { DealsDetailsComponent } from './deals-details/deals-details.component';
import { routing } from './property-deals.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    routing, 
    ReactiveFormsModule,
    FormsModule,
    NgbDropdownModule,
    NgbModalModule,
    SharedModule,
    NgbModule,

  ],
  declarations: [
    PropertyDeals,
    DealsDetailsComponent,
  ],
  entryComponents: [
    
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class PropertyDealsModule { }
