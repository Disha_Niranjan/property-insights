import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { HeaderComponent } from './components/header/header.component';
import { LayoutService } from './layout.service';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular/main';
import { SharedModule } from '../shared/shared.module';
import { CommonModalComponent } from '../shared/common-modal/common-modal.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RolesModalComponent } from './login/roles-modal/roles-modal.component';
import { ChartsModule } from 'ng2-charts';
import { MatMenuModule } from '@angular/material/menu';
@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        TranslateModule,
        NgbDropdownModule,
        FormsModule,
        ReactiveFormsModule, SharedModule,
        NgbModule,
        ChartsModule,
        AgGridModule.withComponents([]),
        MatMenuModule,
    ],
    declarations: [LayoutComponent, HeaderComponent,
        NavbarComponent, RolesModalComponent
    ],
    providers: [
        LayoutService,
        DatePipe
    ],
    entryComponents: [
        CommonModalComponent, RolesModalComponent
    ]
})
export class LayoutModule { }
