import { CommonModule } from '@angular/common';
import { routing } from './dealFinderModule.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/shared/shared.module';
import { DealFinderResultsComponent } from './components/deal-finder/deal-finder-results/deal-finder-results.component';
import { DealFinderComponent } from './components/deal-finder/deal-finder.component';
import { AgGridModule } from 'ag-grid-angular';
import { DealFinderModuleComponent } from './dealFinderModule.component';
import { DealFinderService } from './components/deal-finder/deal-finder.service';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
    CommonModule,
    routing, 
    ReactiveFormsModule,
    FormsModule,
    NgbDropdownModule,
    NgbModalModule,
    SharedModule,
    NgbModule,
    AgGridModule.withComponents([DealFinderResultsComponent]),
    NgxLoadingModule

  ],
  declarations: [
    DealFinderModuleComponent,
    DealFinderComponent,
    DealFinderResultsComponent,
  ],
  providers: [
    DealFinderService
  ],
  entryComponents: [
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class DealFinderModule { }
