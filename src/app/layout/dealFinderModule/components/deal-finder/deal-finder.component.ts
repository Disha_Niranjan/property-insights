import { Component, OnInit } from '@angular/core';
import { faSquare, faCheckSquare } from '@fortawesome/free-solid-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';
import { Router } from '@angular/router';
import { UtilityService } from 'src/app/shared/utitlity.service';

@Component({
  selector: 'app-deal-finder',
  templateUrl: './deal-finder.component.html',
  styleUrls: ['./deal-finder.component.scss']
})
export class DealFinderComponent implements OnInit {

  constructor(private router: Router, private utilService: UtilityService) {
    library.add(faSquare, faCheckSquare);
  }

  ngOnInit() {
  }

  routeToSearchProperty() {
    this.utilService.comeFromDealFinder = true;
    this.router.navigate(['/pages/propertyInsightsModule/dealSearchProperty']);
  }

}
