import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealFinderResultsComponent } from './deal-finder-results.component';

describe('DealFinderResultsComponent', () => {
  let component: DealFinderResultsComponent;
  let fixture: ComponentFixture<DealFinderResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DealFinderResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealFinderResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
