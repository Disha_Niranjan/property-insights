import { Component, OnInit } from '@angular/core';
import { GridApi, ICellRendererParams } from 'ag-grid-community';
import { AgGridAngular } from 'ag-grid-angular';
import { DealFinderService } from '../deal-finder.service';
import { ApplicationHttpClient } from 'src/app/shared/guard/ApplicationHttpClient.service';
import { HttpRequest, HttpHeaders, HttpClient } from '@angular/common/http';
import xml2js from 'xml2js';
import { NgxXml2jsonService } from 'ngx-xml2json';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { LinkRendererComponent } from 'src/app/shared/components/renderers/link-renderer/link-renderer.component';
import { CommonModalComponent } from 'src/app/shared/common-modal/common-modal.component';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';

class RowDetails {
  // processType: string;
  image: any;
  displayable_address: any;
  desc: any;
  num_bedrooms: any;
  price: any;
  avgPrice: any;
  property_type: any;
  baths: any;
  town: any;
  size: any;
  link: any;
  postcode: any;

}
class DealSearchData {
  build_type: any;
  district: any;
  numberhabitablerooms: any;
  postcode: any;
  prop_type: any;
  town_city: any;
  buildYear: any;
  currentCondition: any;
  numberOfBaths: any;
  numberOfGarage: any;
  typeOfGarden: any;

}

class Results {
  address1: any;
  avgprcbitfarareapsqm: any;
  avgprcexactareapsqm: any;
  avgprcfarareapsqm: any;
  avgprcnearbyareapsqm: any;
  avgprice: any;
  avgweightage: any;
  bitfarpostcode: any;
  bitfarvaluation: any;
  builtform: any;
  confidenceLevel: any;
  dataPointAssessed: any;
  dateoftrn: any;
  exactpostcode: any;
  exactvaluation: any;
  farpostcode: any;
  farvaluation: any;
  lastSoldPrice: any;
  nearbypostcode: any;
  nearbyvaluation: any;
  numberhabitablerooms: any;
}
class rowselData {
  rowselectedaddress: any;
  rowselecteddesc: any;
  rowselectedrooms: any;
  rowselectedpropertytype: any;
  rowselectedprice: any
}
@Component({
  selector: 'app-deal-finder-results',
  templateUrl: './deal-finder-results.component.html',
  styleUrls: ['./deal-finder-results.component.scss']
})
export class DealFinderResultsComponent implements OnInit {

  dealfinderList: any[];
  rowDataList: RowDetails[] = [];
  selectedRowDataList: any[] = [];
  xmlItems: any;
  gridApi: GridApi;
  columnDefs: any;
  context;
  frameworkComponents;
  rowData$: Observable<RowDetails>;
  partialpostcode: boolean;
  searchResults: any;
  listingList: any;
  room: any;
  loading: boolean = false;
  searchdata: any;

  constructor(
    private service: DealFinderService,
    private http: HttpClient, private router: Router,
    private ngxXml2jsonService: NgxXml2jsonService, private modalService: NgbModal,
  ) {
    window.scrollTo(0, 0);
    this.searchdata = JSON.parse(sessionStorage.getItem('DealSearchFormData'));
    // this.service.getAllDealFinderList().subscribe(res => {
    this.service.getDealSearchList(this.searchdata).subscribe(res => {
      this.listingList = res
      this.initGrid(true);
      console.log(this.listingList)

      // read XML file
      // const parser = new DOMParser();
      // const xml = parser.parseFromString(res, 'text/xml');
      // const obj = this.ngxXml2jsonService.xmlToJson(xml);
      // const values = Object.keys(obj).map(response => obj[response]);
      // this.listingList = values[0].listing;

      this.listingList.forEach(res1 => {

        const rowValues = new RowDetails();
        rowValues.displayable_address = res1.address;
        rowValues.num_bedrooms = res1.numOfBedrooms;
        rowValues.price = res1.absolutePrice;
        rowValues.desc = res1.shortDesc;
        rowValues.property_type = res1.propertyType;
        rowValues.desc = res1.shortDesc;
        rowValues.link = res1.details_url;
        rowValues.baths = res1.numOfBath;
        rowValues.town = res1.postTown;
        rowValues.image = res1.thumbnail;

        // rowValues.size = res1.price * .005;
        // rowValues.postcode = res1.outcode;
        // rowValues.image = res1.detailsUrl;
        // rowValues.avgPrice = '-';

        this.rowDataList.push(rowValues);

      });
      this.gridApi.setRowData(this.rowDataList);

    });
    this.ngOnInit();
    this.frameworkComponents = {
      linkRendererComponent: LinkRendererComponent,
    }
  }

  ngOnInit() {

    this.initGrid(true);

  }


  initGrid(selectable: boolean) {

    this.columnDefs = [
      { headerName: '', width: 83, field: 'image', cellRenderer: (params: ICellRendererParams) => {
        // here the data is the row object you need
        return ` <img style="height: 45px; width: 80px;" src="${params.value}">`;
      }
    },
      { headerName: 'Address', field: 'displayable_address', sortable: true, resizable: true, width: 310, cellStyle: {textAlign: 'left'} },
      { headerName: 'Town', field: 'town', sortable: true, width: 150 },
      { headerName: 'Beds', field: 'num_bedrooms', sortable: true, width: 100 },
      { headerName: 'Asking Price', field: 'price', sortable: true, width: 150 },
      // { headerName: 'Avg Price', field: 'avgPrice', sortable: true, width: 150 },
      { headerName: 'Type', field: 'property_type', sortable: true, width: 200 },
      { headerName: 'Bath', field: 'baths', sortable: true, width: 100 },
      {
        headerName: 'Property Link', field: 'link',
        cellRenderer: (params: ICellRendererParams) => {
          // here the data is the row object you need
          return `<a style="color: blue;    padding: 5px 10px;
          font-family: 'Arimo', sans-serif;
          color: #fff;
          background: #777c77;
          font-size: 11px;
          text-decoration: none;line-height: 15px;
          border-radius: 25px;">INSIGHT</a>`;
        }
      }

    ];
  }

  
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
    this.gridApi.setGridAutoHeight(true);
    this.gridApi.hideOverlay();
  }

  showAlert(flag: boolean): NgbModalRef {
    const activeModal = this.modalService.open(CommonModalComponent, { size: 'sm' });
    activeModal.componentInstance.showHide = flag;
    activeModal.componentInstance.modalHeader = 'Info';
    activeModal.componentInstance.oKMessage = 'Ok';
    return activeModal;
  }
  onRowSelected(event: any) {
    // this.loading = true

    this.selectedRowDataList = this.gridApi.getSelectedNodes();
    console.log(this.selectedRowDataList[0].data)
    let paramValues = new RowDetails();
    paramValues = this.selectedRowDataList[0].data;

    console.log(paramValues)
    const searchData = new DealSearchData();
    searchData.district = "Harrow";
    searchData.postcode = "";
    searchData.town_city = "Harrow";
    searchData.numberhabitablerooms = paramValues.num_bedrooms;
    searchData.build_type = "";
    searchData.buildYear = "";
    searchData.currentCondition = "";
    searchData.numberOfBaths = "";
    searchData.numberOfGarage = "";
    searchData.typeOfGarden = "";
    searchData.prop_type = "House";
    // this.dataList = params;
    // console.log(this.dataList)
    // if (paramValues.postcode.length < 7) {
    //   this.partialpostcode = true;

    //   console.log(this.partialpostcode)
    // } else {
    //   this.partialpostcode = false;
    //   console.log(this.partialpostcode)

    // }
    const selData = new rowselData();

    selData.rowselectedaddress = paramValues.displayable_address;
    selData.rowselecteddesc = paramValues.desc;
    selData.rowselectedrooms = paramValues.num_bedrooms;
    selData.rowselectedpropertytype = '-';
    selData.rowselectedprice = paramValues.price;

    this.router.navigate(['pages/propertyInsightsModule/dealPropertyInsights/', {...selData }]);


    // this.service.getProperyEvaluation(searchData).subscribe((results: any) => {
    //   this.loading = true
    //   let interval = setInterval(() => {

    //     setTimeout(() => {
    //       clearInterval(interval);
    //       if (!results) {
    //         const activeModal = this.showAlert(true);
    //         activeModal.componentInstance.modalContent = 'No Results Found';
    //       }
    //     }, 60000);

    //     if (!results.details) {
    //       debugger
    //       clearInterval(interval);

    //       if (!results.details) {
    //         for (let key of Object.keys(results)) {
    //           results[key] = results[key] && results[key] != 0 ? results[key] : 'NA';
    //         }

    //         if (!this.partialpostcode) {
    //           results.avgprice = results.propertyInsightvaluation;
    //         }
    //         this.router.navigate(['pages/propertyInsightsModule/dealPropertyInsights/', { ...results, ...selData }]);

    //       }
    //     }
    //   }, 1000);
    //   this.loading = false;
    // }, error => {
    //   const activeModal = this.showAlert(false);
    //   activeModal.componentInstance.modalContent = 'Internal Server Error!';
    //   this.loading = false;
    // })


  }

}
