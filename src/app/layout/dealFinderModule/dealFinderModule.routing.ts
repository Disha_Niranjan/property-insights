import { Routes, RouterModule } from '@angular/router';
import { DealFinderResultsComponent} from './components/deal-finder/deal-finder-results/deal-finder-results.component';
import { DealFinderComponent } from './components/deal-finder/deal-finder.component';
import { DealFinderModuleComponent } from './dealFinderModule.component';

const routes: Routes = [
  {
    path: '',
    component: DealFinderModuleComponent,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dealFinder', component: DealFinderComponent },
      { path: 'dealFinderList', component: DealFinderResultsComponent },
    ]
  }
];

export const routing = RouterModule.forChild(routes);
