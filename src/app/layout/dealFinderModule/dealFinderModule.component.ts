import {Component} from '@angular/core';

@Component({
  selector: 'dealFinderModule',
  template: `<router-outlet></router-outlet>`
})
export class DealFinderModuleComponent {
  constructor() {
  }
}
