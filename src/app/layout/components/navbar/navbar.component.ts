import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CommonModalComponent } from 'src/app/shared/common-modal/common-modal.component';
import { LoginModalComponent } from '../../login/login-modal/login-modal.component';
import { AuthService } from 'src/app/shared/guard';
import { Observable } from 'rxjs';
import { UtilityService } from 'src/app/shared/utitlity.service';
import { SignUpModalComponent } from '../../login/signUp-modal/signUp-modal.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  subscribedUser: any;
  // isloggedin = false;
  isLoggedIn$: Observable<boolean>;
  isLoggedIn: any;

  isLogin: boolean;
  constructor(public router: Router, private modalService: NgbModal, private authservice: AuthService,
    public utilService: UtilityService) {
    this.utilService.isUserLogin = sessionStorage.getItem('credentials') ? true : false;
  }

  ngOnInit() {
    this.isLoggedIn$ = this.authservice.isLoggedIn;
    //  if (this.authservice.isLoggedIn ) {
    //     this.isloggedin = true;
    //     console.log(this.isloggedin)
    //  } else {
    //    this.isloggedin = false;
    //  }
  }
  wplogin() {
    return this.router.navigate(['pages/login/wplogin']);

  }
  public login() {

    const activeModal = this.modalService.open(LoginModalComponent, { size: 'sm' });
    activeModal.componentInstance.showHide = true;
    activeModal.componentInstance.modalHeader = 'Sign In';
    activeModal.result.then(res => {
      // this.isLogin = res;
      // this.utilService.isUserLogin = res;
    }, error => {
      console.log(error)
    });
    // activeModal.result.then(res => {
    //   if (res == 'Y') {
    //     this.router.navigate(['pages/login']);
    //   } else {

    //   }
    // });
  }

  logout() {
    sessionStorage.clear();
    this.isLogin = false;
    this.utilService.isUserLogin = false;

    this.authservice.logout();
  }

  dealFinder() {
    this.subscribedUser = sessionStorage.getItem('userRole');
    console.log('role is' + ' ' + this.subscribedUser)
    if (this.subscribedUser == 'ADMIN' || this.subscribedUser == 'USER') {
      return this.router.navigate(['pages/dealFinderModule/dealFinder']);
    } else if (this.subscribedUser == 'FREE') {
      const activeModal = this.showAlert(true);
      activeModal.componentInstance.modalContent = 'You are not subsribed to Deal Finder service';
      activeModal.result.then(res => {
        if (res == 'Y') {
          return this.router.navigate(['pages/login']);
        } else {
          // this.login();
        }
      });
    } else {
      this.login();
    }
  }

  showAlert(flag: boolean): NgbModalRef {
    const activeModal = this.modalService.open(CommonModalComponent, { size: 'sm' });
    activeModal.componentInstance.showHide = flag;
    activeModal.componentInstance.modalHeader = 'Message';
    return activeModal;
  }

  propertyAnalyzer() {
    this.subscribedUser = sessionStorage.getItem('userRole');
    if (this.subscribedUser == 'ADMIN') {
      return this.router.navigate(['pages/propertyAnalyzer/propertyDetails']);
      debugger
    } else if (this.subscribedUser == 'USER' || this.subscribedUser == 'FREE') {
      const activeModal = this.showAlert(true);
      activeModal.componentInstance.modalContent = 'You are not subsribed to Property Analyzer service';
      activeModal.result.then(res => {
        if (res == 'Y') {
          return this.router.navigate(['pages/login']);
        } else {
          // this.login();
        }
      });
    } else {
      this.login();
    }
  }

  propertyDeals() {
    this.router.navigate(['pages/propertyDeals/dealsDetails']);
  }

  // propertyInsights() {

  //   this.subscribedUser = sessionStorage.getItem('userRole');
  //   if (this.subscribedUser == 'ADMIN') {
  //     this.utilService.comeFromDealFinder = false;
  //     console.log('the value is : ' + this.utilService.comeFromDealFinder);
  //     return this.router.navigate(['pages/propertyInsightsModule/searchProperty']);
  //   } else if (this.subscribedUser == 'USER' || this.subscribedUser == 'FREE') {
  //     const activeModal = this.showAlert(true);
  //     activeModal.componentInstance.modalContent = 'You are not subsribed to Property Insights service';
  //     activeModal.result.then(res => {
  //       if (res == 'Y') {
  //         return this.router.navigate(['pages/login']);
  //       } else {
  //       }
  //     });
  //   } else {
  //     this.login();
  //   }

  // }

  propertyInsights() {
    if (this.utilService.isUserLogin) {
      this.subscribedUser = sessionStorage.getItem('userRole');
      if (this.subscribedUser == 'ADMIN') {
        this.utilService.comeFromDealFinder = false;
        console.log('the value is : ' + this.utilService.comeFromDealFinder);
        return this.router.navigate(['pages/propertyInsightsModule/searchProperty']);
      } else if (this.subscribedUser == 'user' || this.subscribedUser == 'free') {
        const activeModal = this.showAlert(true);
        activeModal.componentInstance.modalContent = 'You are not subsribed to Property Insights service';
        activeModal.result.then(res => {
          if (res == 'Y') {
            return this.router.navigate(['pages/login']);
          } else {
          }
        });
      }

    } else {
      this.login();
    }

  }


  signup() {
    const activeModal = this.modalService.open(SignUpModalComponent, { size: 'sm' });
    activeModal.componentInstance.showHide = true;
    activeModal.componentInstance.modalHeader = false;
    return activeModal;
  }
}
