import { Routes, RouterModule } from '@angular/router';
import { PropertyInsightsModuleComponent } from './propertyInsightsModule.component';
import { PropertyInsightsComponent} from './components/property-insights/property-insights.component';
import { SearchPropertyComponent } from './components/search-property/search-property.component';
import { DealPropertyInsightsComponent } from './components/deal-property-insights/deal-property-insights.component';
import { DealSearchPropertyComponent } from './components/deal-search-property/deal-search-property.component';

const routes: Routes = [
  {
    path: '',
    component: PropertyInsightsModuleComponent,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'propertyInsights', component: PropertyInsightsComponent},
      { path: 'searchProperty', component: SearchPropertyComponent},
      { path: 'dealPropertyInsights', component: DealPropertyInsightsComponent},
      { path: 'dealSearchProperty', component: DealSearchPropertyComponent},


    ]
  }
];

export const routing = RouterModule.forChild(routes);
