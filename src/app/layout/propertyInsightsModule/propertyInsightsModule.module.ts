import { CommonModule } from '@angular/common';
import { routing } from './propertyInsightsModule.routing';
import { NgbModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/shared/shared.module';
import { AgGridModule } from 'ag-grid-angular';
import { PropertyInsightsModuleComponent } from './propertyInsightsModule.component';
import { PropertyInsightsComponent, RoundPipe } from './components/property-insights/property-insights.component';
import { SearchPropertyComponent } from './components/search-property/search-property.component';
import { MatSliderModule } from '@angular/material/slider';
import { MatCardModule, MatGridListModule, MatExpansionModule, MatListModule, MatCheckboxModule } from '@angular/material';
import { ChartsModule as Ng2Charts, ChartsModule } from 'ng2-charts';
import { AgmCoreModule } from '@agm/core';
import { PropertyInsightsService } from './components/property-insights/property-insights.service';
import { SearchPropertyService } from './components/search-property/search-property.service';
import { DealPropertyInsightsService } from './components/deal-property-insights/deal-property-insights.service';
import { DealPropertyInsightsComponent } from './components/deal-property-insights/deal-property-insights.component';
import { NgxLoadingModule } from 'ngx-loading';
import { DealSearchPropertyComponent } from './components/deal-search-property/deal-search-property.component';
import { DealSearchPropertyService } from './components/deal-search-property/deal-search-property.service';
import { DealFinderService } from '../dealFinderModule/components/deal-finder/deal-finder.service';
import { NgApexchartsModule } from 'ng-apexcharts';

@NgModule({
  imports: [
    CommonModule,
    routing,
    ReactiveFormsModule,
    FormsModule,
    NgbDropdownModule,
    NgbModalModule,
    SharedModule,
    NgbModule,
    AgGridModule.withComponents([]),
    MatSliderModule,
    MatCardModule,
    ChartsModule,
    AgmCoreModule,
    NgxLoadingModule,
    NgbTabsetModule,
    MatGridListModule,
    MatExpansionModule,
    MatListModule,
    NgApexchartsModule,
    MatCheckboxModule
  ],
  declarations: [
    PropertyInsightsModuleComponent,
    PropertyInsightsComponent,
    DealPropertyInsightsComponent,
    SearchPropertyComponent,
    DealSearchPropertyComponent,
    RoundPipe
  ],
  providers: [
    PropertyInsightsService,
    SearchPropertyService,
    DealPropertyInsightsService,
    DealSearchPropertyService,
    DealFinderService,
    RoundPipe
  ],
  entryComponents: [
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class PropertyInsightsModule { }
