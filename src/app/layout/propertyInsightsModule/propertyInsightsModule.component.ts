import {Component} from '@angular/core';

@Component({
  selector: 'propertyInsightsModule',
  template: `<router-outlet></router-outlet>`
})
export class PropertyInsightsModuleComponent {
  constructor() {
  }
}
