import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DealSearchPropertyComponent } from './deal-search-property.component';

describe('DealSearchPropertyComponent', () => {
  let component: DealSearchPropertyComponent;
  let fixture: ComponentFixture<DealSearchPropertyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DealSearchPropertyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealSearchPropertyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
