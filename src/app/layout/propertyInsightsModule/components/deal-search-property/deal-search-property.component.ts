import { Component, OnInit } from '@angular/core';
import { faSquare, faCheckSquare } from '@fortawesome/free-solid-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';
import { ActivatedRoute, Params, Router, NavigationEnd } from '@angular/router';
import { coerceNumberProperty } from '@angular/cdk/coercion';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { UtilityService } from 'src/app/shared/utitlity.service';
import { DealSearchPropertyService } from './deal-search-property.service';
import { MatDialog } from '@angular/material/dialog';
import { CommonModalComponent } from 'src/app/shared/common-modal/common-modal.component';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';

class SearchData {
  town_city: any;
  keywords: any;
  listening_status: any;
  postcode: any;
  property_type: any;
  maximum_price: any;
  radius: any;
  minimum_price: any;
  orderBy: any;
  page_size: any;
  summarised: any;
  // results: any = [];
}


@Component({
  selector: 'app-deal-search-property',
  templateUrl: './deal-search-property.component.html',
  styleUrls: ['./deal-search-property.component.scss']
})

export class DealSearchPropertyComponent implements OnInit {
  searchPropertyForm: FormGroup;

  public selectCity: AbstractControl;
  public postcode: AbstractControl;
  public selectType: AbstractControl;
  public radius: AbstractControl;
  // public noOfrooms: AbstractControl;
  // public priceOrValuation: AbstractControl;
  // public garage: AbstractControl;
  // public noOfBaths: AbstractControl;
  // public garden: AbstractControl;
  // public condition: AbstractControl;


  // public minvalue: AbstractControl;
  // public maxvalue: AbstractControl;
  loading: boolean = false;
  dataList: any;
  currentJustify = 'fill';
  autoTicks = false;
  showTicks = false;
  step = 1;
  thumbLabel = false;
  minvalue = 50000;
  maxvalue = 1000000;
  vertical = false;
  pathfromdealfinder: boolean;
  previousUrl: string;
  currentUrl: string;
  successMessage: boolean = true;
  color: 'primary';
  mode: 'determinate';
  value: 40;
  status = 'success';
  searchResults: any;

  get tickInterval(): number | 'auto' {
    return this.showTicks ? (this.autoTicks ? 'auto' : this._tickInterval) : 0;
  }
  set tickInterval(value) {
    this._tickInterval = coerceNumberProperty(value);
  }
  private _tickInterval = 1;

  constructor(private route: ActivatedRoute, private location: Location,
     private modalService: NgbModal, private fb: FormBuilder, private service: DealSearchPropertyService,
              private router: Router,
              private utilService: UtilityService,
  ) {
    library.add(faSquare, faCheckSquare);
    this.createForm();


  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.dataList = params;
    });


  }
  showAlert(flag: boolean): NgbModalRef {
    const activeModal = this.modalService.open(CommonModalComponent, { size: 'sm' });
    activeModal.componentInstance.showHide = flag;
    activeModal.componentInstance.modalHeader = 'Info';
    activeModal.componentInstance.oKMessage = 'Ok';
    return activeModal;
  }
  search() {
    if (this.searchPropertyForm.valid) {

      const formValues = new SearchData();

      // formValues.build_type = this.searchPropertyForm.controls['reportType'];
      formValues.town_city = this.searchPropertyForm.controls['selectCity'].value;
      formValues.postcode = this.searchPropertyForm.controls['postcode'].value;
      formValues.property_type = this.searchPropertyForm.controls['selectType'].value;
      formValues.radius = this.searchPropertyForm.controls['radius'].value;

      formValues.keywords = "";
      formValues.listening_status = "sale";
      formValues.maximum_price = "";
      formValues.minimum_price = "";
      formValues.orderBy = "";
      formValues.page_size = "";
      formValues.summarised = "";
      // formValues.currentCondition = this.searchPropertyForm.controls['condition'].value;
      // formValues.numberOfBaths = this.searchPropertyForm.controls['noOfBaths'].value;
      // formValues.numberOfGarage = this.searchPropertyForm.controls['garage'].value;
      // formValues.typeOfGarden = this.searchPropertyForm.controls['garden'].value;
     

      // this.loading = true;
      sessionStorage.setItem('DealSearchFormData' , JSON.stringify(formValues))

      // this.service.getDealSearchList(formValues).subscribe(res => {

      //   this.searchResults = res;

      //   console.log(this.searchResults)

      // })
      this.router.navigate(['/pages/dealFinderModule/dealFinderList']);


      // this.service.getProperyEvaluation(formValues).subscribe(results => {
      //   let interval = setInterval(() => {

      //     setTimeout(() => {
      //       clearInterval(interval);
      //       if (!results) {
      //         const activeModal = this.showAlert(true);
      //         activeModal.componentInstance.modalContent = 'No Results Found';
      //       }
      //     }, 60000);

      //     if (results) {
      //       debugger
      //       clearInterval(interval);
      //       if (results.message) {
      //         const activeModal = this.showAlert(false);
      //         activeModal.componentInstance.modalContent = 'No Results Found';
      //       } else {
      //         const jsonValue = JSON.stringify(results);
      //         // console.log('json' + jsonValue)
      //         const valueFromJson = JSON.parse(jsonValue);  
      //         // console.log('parse'+ valueFromJson)  
      //         // this.formValues.results.push(jsonValue);
      //         // console.log('formvalue'+ this.formValues)
      //         const res = {...formValues , ...results}

      //         // console.log('res..'+ res.town_city)
      //         // this.router.navigate(['/pages/propertyInsightsModule/propertyInsights',  {...formValues , ...results}]);
      //         sessionStorage.setItem('searchFormData' , JSON.stringify(formValues))
      //         console.log('dataaahdjgdhskdsaujcgsau'+ sessionStorage.getItem('searchFormData'))
      //         this.router.navigate(['/pages/propertyInsightsModule/propertyInsights', results ] );
      //       }
      //     }
      //   }, 1000);
      //   this.loading = false;
      // }, error => {
      //   const activeModal = this.showAlert(false);
      //   activeModal.componentInstance.modalContent = 'Internal Server Error!';
      //   this.loading = false;
      // })

    } else {
      console.log('invalid form')
    }
  }

  private createForm() {
    this.searchPropertyForm = this.fb.group({
      'selectCity': ['', Validators.required],
      'postcode': ['', Validators.required],
      'selectType': ['', Validators.required],
      'minvalue': [''],
      'maxvalue': [''],
      'radius': [''],
      // 'noOfrooms': ['', Validators.required],
      // 'priceOrValuation': ['', Validators.required],
      // 'garage': [''],
      // 'noOfBaths': [''],
      // 'garden': [''],
      // 'condition': ['']

    });

    this.selectCity = this.searchPropertyForm.controls['selectCity'];
    this.postcode = this.searchPropertyForm.controls['postcode'];
    this.selectType = this.searchPropertyForm.controls['selectType'];
    this.radius = this.searchPropertyForm.controls['radius'];
    // this.noOfrooms = this.searchPropertyForm.controls['noOfrooms'];
    // this.priceOrValuation = this.searchPropertyForm.controls['priceOrValuation'];
    // this.garage = this.searchPropertyForm.controls['garage'];
    // this.noOfrooms = this.searchPropertyForm.controls['noOfrooms'];
    // this.garden = this.searchPropertyForm.controls['garden'];
    // this.condition = this.searchPropertyForm.controls['condition'];
    // this.noOfBaths = this.searchPropertyForm.controls['noOfBaths'];



    // this.minvalue = this.searchPropertyForm.controls['minvalue'];
    // this.maxvalue = this.searchPropertyForm.controls['maxvalue'];

  }

}
