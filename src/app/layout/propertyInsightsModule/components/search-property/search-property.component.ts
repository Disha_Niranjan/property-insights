import { Component, OnInit } from '@angular/core';
import { faSquare, faCheckSquare } from '@fortawesome/free-solid-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';
import { ActivatedRoute, Params, Router, NavigationEnd } from '@angular/router';
import { coerceNumberProperty } from '@angular/cdk/coercion';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { UtilityService } from 'src/app/shared/utitlity.service';
import { SearchPropertyService } from './search-property.service';
import { MatDialog } from '@angular/material/dialog';
import { CommonModalComponent } from 'src/app/shared/common-modal/common-modal.component';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';

class SearchData {
  buildYear: any;
  currentCondition: any;
  district: any;
  numberOfBaths: any;
  numberOfGarage: any;
  bedrooms: any;
  receptions: any;
  postcode: any;
  prop_type: any;
  town_city: any;
  typeOfGarden: any;
  address: any;
  build_type: any;
  internalarea: any;
  priceOrValuation: any;
  parking: any;
  // results: any = [];
}


@Component({
  selector: 'app-search-property',
  templateUrl: './search-property.component.html',
  styleUrls: ['./search-property.component.scss']
})

export class SearchPropertyComponent implements OnInit {
  searchPropertyForm: FormGroup;

  public selectCity: AbstractControl;
  public postcode: AbstractControl;
  public selectType: AbstractControl;
  public constructionDate: AbstractControl;
  public noOfrooms: AbstractControl;
  public priceOrValuation: AbstractControl;
  public garage: AbstractControl;
  public noOfBaths: AbstractControl;
  public garden: AbstractControl;
  public condition: AbstractControl;
  public address: AbstractControl;
  public inputAddress: AbstractControl;
  public parking: AbstractControl;
  public receptions: AbstractControl;
  public internalarea: AbstractControl;

  // public minvalue: AbstractControl;
  // public maxvalue: AbstractControl;
  loading: boolean = false;
  dataList: any;
  currentJustify = 'fill';
  autoTicks = false;
  showTicks = false;
  step = 1;
  thumbLabel = false;
  minvalue = 50000;
  maxvalue = 1000000;
  vertical = false;
  pathfromdealfinder: boolean;
  previousUrl: string;
  currentUrl: string;
  postcodeMessage: string;
  status = 'warning';

  color: 'primary';
  mode: 'determinate';
  value: 40;
  addresslist: any;
  bedroomlist: any;
  propertyTypeList: any;
  citiesList: any;
  ispostcodevalid: boolean = false;
  postcodenotfound: boolean = false;
  addressnotlisted: boolean = false;
  filledRequiredFields: boolean = false;
  postcodeError: any;
  searchedPostcode: any;
  internalArea: any;

  get tickInterval(): number | 'auto' {
    return this.showTicks ? (this.autoTicks ? 'auto' : this._tickInterval) : 0;
  }
  set tickInterval(value) {
    this._tickInterval = coerceNumberProperty(value);
  }
  private _tickInterval = 1;

  constructor(private route: ActivatedRoute,
    private location: Location, private modalService: NgbModal,
    private fb: FormBuilder,
    private router: Router,
    private utilService: UtilityService,
    private service: SearchPropertyService,
  ) {
    library.add(faSquare, faCheckSquare);
    this.createForm();
    this.service.getCities().subscribe(city => {
      this.citiesList = city.cities;
    })

  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.dataList = params;
    });

  }
  showAlert(flag: boolean): NgbModalRef {
    const activeModal = this.modalService.open(CommonModalComponent, { size: 'sm' });
    activeModal.componentInstance.showHide = flag;
    activeModal.componentInstance.modalHeader = 'Info';
    activeModal.componentInstance.oKMessage = 'Ok';
    return activeModal;
  }
  search() {
    if (this.utilService.comeFromDealFinder) {
      // this.router.navigate(['/pages/dealFinderModule/dealFinderList']);
    } else if (this.searchPropertyForm.valid) {

      const formValues = new SearchData();

      // if (this.postcodenotfound || this.addressnotlisted) {
      if (this.addressnotlisted) {
        formValues.address = this.searchPropertyForm.controls['inputAddress'].value;
      } else {
        formValues.address = this.searchPropertyForm.controls['address'].value;
      }
      formValues.build_type = "";
      formValues.district = this.searchPropertyForm.controls['selectCity'].value;
      formValues.postcode = this.searchPropertyForm.controls['postcode'].value;
      formValues.prop_type = this.searchPropertyForm.controls['selectType'].value;
      formValues.town_city = this.searchPropertyForm.controls['selectCity'].value;
      formValues.bedrooms = parseInt(((this.noOfrooms.value === null || this.noOfrooms.value === "") ? 0 : this.noOfrooms.value).toString());
      formValues.buildYear = this.searchPropertyForm.controls['constructionDate'].value;
      formValues.currentCondition = this.searchPropertyForm.controls['condition'].value;
      formValues.numberOfBaths = parseInt(((this.noOfBaths.value === null || this.noOfBaths.value === "") ? 0 : this.noOfBaths.value).toString());
      formValues.numberOfGarage = parseInt(((this.garage.value === null || this.garage.value === "") ? 0 : this.garage.value).toString());
      formValues.typeOfGarden = this.searchPropertyForm.controls['garden'].value;
      formValues.internalarea = this.searchPropertyForm.controls['internalarea'].value;
      formValues.parking = this.searchPropertyForm.controls['parking'].value;
      formValues.priceOrValuation = this.searchPropertyForm.controls['priceOrValuation'].value;
      formValues.receptions = parseInt(((this.receptions.value === null || this.receptions.value === "") ? 0 : this.receptions.value).toString());

      this.loading = true;

      this.service.getProperyEvaluation(formValues).subscribe(results => {
        let interval = setInterval(() => {

          if (results) {
            debugger
            clearInterval(interval);
            if (results.message) {
              const activeModal = this.showAlert(false);
              activeModal.componentInstance.modalContent = 'No Results Found';
            } else {
              const jsonValue = JSON.stringify(results);
              const valueFromJson = JSON.parse(jsonValue);
              // console.log('parse'+ valueFromJson)  
              // this.formValues.results.push(jsonValue);
              // console.log('formvalue'+ this.formValues)
              const res = { ...formValues, ...results }

              // console.log('res..'+ res.town_city)
              // this.router.navigate(['/pages/propertyInsightsModule/propertyInsights',  {...formValues , ...results}]);
              sessionStorage.setItem('searchFormData', JSON.stringify(formValues))
              this.router.navigate(['/pages/propertyInsightsModule/propertyInsights', results]);
            }
          }
        }, 1000);
        this.loading = false;
      }, error => {
        const activeModal = this.showAlert(false);
        activeModal.componentInstance.modalContent = error;
        this.loading = false;
      })

    } else {
      if (!this.inputAddress.valid) {
        this.filledRequiredFields = true;
      }
    }
  }
  loadAddress() {
    let postcode = this.searchPropertyForm.controls['postcode'].value;

    this.loading = true;
    this.service.getloadAddress(postcode).subscribe(results => {
      let interval = setInterval(() => {

        // setTimeout(() => {
        //   clearInterval(interval);
        //   if (!results) {
        //     const activeModal = this.showAlert(true);
        //     activeModal.componentInstance.modalContent = 'No Results Found';
        //   }
        // }, 60000);

        if (results) {
          clearInterval(interval);
          if (results.details == 200) {
            // const activeModal = this.showAlert(false);
            // activeModal.componentInstance.modalContent = results.message;
            // this.postcodenotfound = true;
            this.ispostcodevalid = false;

            this.onMessage(results.message);
          } else {

            this.searchedPostcode = this.postcode.value;
            this.noOfrooms.setValue('');
            this.address.setValue('');
            console.log(this.address.value);
            this.selectType.setValue('');
            this.inputAddress.reset();
            this.addresslist = results.address;
            this.bedroomlist = results.bedrooms;
            this.propertyTypeList = results.property_type;
            this.ispostcodevalid = true;
            // this.postcodenotfound = false;

          }
        }

      }, 1000);
      this.loading = false;
    }, error => {
      const activeModal = this.showAlert(false);
      console.log(error);
      activeModal.componentInstance.modalContent = 'Internal Server Error!';
      this.loading = false;
      // this.postcodenotfound = true;

    });

  }
  onMessage(msg: string) {
    this.postcodeMessage = msg;
    this.selectType.setValue('');
    this.noOfrooms.setValue('');
    setTimeout(() => this.postcodeMessage = null, 10000);
    this.status = 'warning';
  }
  subscribePage() {
    this.router.navigate(['pages/login']);
  }
  signin() {

  }


  private createForm() {
    this.searchPropertyForm = this.fb.group({
      'selectCity': ['', Validators.required],
      'postcode': ['', Validators.compose([Validators.required, Validators.pattern('^(([A-Z]{1,2}[0-9][A-Z0-9]?|ASCN|STHL|TDCU|BBND|[BFS]IQQ|PCRN|TKCA) ?[0-9][A-Z]{2}|BFPO ?[0-9]{1,4}|(KY[0-9]|MSR|VG|AI)[ -]?[0-9]{4}|[A-Z]{2} ?[0-9]{2}|GE ?CX|GIR ?0A{2}|SAN ?TA1)$')])],
      'selectType': [''],
      'address': ['', Validators.required],
      'constructionDate': [''],
      'noOfrooms': [''],
      'priceOrValuation': [''],
      'garage': [''],
      'noOfBaths': [''],
      'garden': [''],
      'condition': [''],
      'inputAddress': [''],
      'parking': [''],
      'receptions': [''],
      'internalarea': ['']


    });

    this.selectCity = this.searchPropertyForm.controls['selectCity'];
    this.postcode = this.searchPropertyForm.controls['postcode'];
    this.address = this.searchPropertyForm.controls['address'];
    this.selectType = this.searchPropertyForm.controls['selectType'];
    this.constructionDate = this.searchPropertyForm.controls['constructionDate'];
    this.noOfrooms = this.searchPropertyForm.controls['noOfrooms'];
    this.priceOrValuation = this.searchPropertyForm.controls['priceOrValuation'];
    this.garage = this.searchPropertyForm.controls['garage'];
    this.garden = this.searchPropertyForm.controls['garden'];
    this.condition = this.searchPropertyForm.controls['condition'];
    this.noOfBaths = this.searchPropertyForm.controls['noOfBaths'];
    this.inputAddress = this.searchPropertyForm.controls['inputAddress'];
    this.parking = this.searchPropertyForm.controls['parking'];
    this.internalarea = this.searchPropertyForm.controls['internalarea'];
    this.receptions = this.searchPropertyForm.controls['receptions'];

    this.address.valueChanges.subscribe(val => {
      this.internalarea.reset();
      this.inputAddress.reset();
      if (val == 'notlisted') {
        this.addressnotlisted = true;
        this.internalarea.setValidators(this.internalarea ? Validators.required : Validators.nullValidator);
        this.internalarea.updateValueAndValidity();
      } else {
        this.addressnotlisted = false;
        let values = {
          address: this.address.value,
          postcode: this.postcode.value
        };
        console.log(values);
        this.service.getInternalArea(values).subscribe(area => {
          this.internalArea = area;
        });
      }
      this.inputAddress.setValidators(this.addressnotlisted ? Validators.required : Validators.nullValidator);
      this.inputAddress.updateValueAndValidity();

    });
  }

}
