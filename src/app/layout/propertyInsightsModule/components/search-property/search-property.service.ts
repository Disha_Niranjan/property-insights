import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ApplicationHttpClient } from 'src/app/shared/guard/ApplicationHttpClient.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map, catchError, timeout } from 'rxjs/operators';
import { throwError } from 'rxjs';
@Injectable()
export class SearchPropertyService {

  constructor(private http: ApplicationHttpClient, ) { }

  getProperyEvaluation(params): Observable<any> {
    return this.http.Post('v1/property_evaluation', params).pipe(
      timeout(300000),
      catchError(err => {
        if (err.name == 'TimeoutError') {
          return throwError('No Results Found');

        } else if (err.name == 'HttpErrorResponse') {
          return throwError('Internal Server Error');

        } else {
          return throwError('No Results Found');

        }
      })
    );
  }

  getloadAddress(code: any): Observable<any> {

    return this.http.Get(`v1/address/${code}`);
  }

  getCities(): Observable<any> {
    return this.http.Get('v1/getcities');
  }
  getInternalArea(params): Observable<any> {
    return this.http.Post('v1/getInternalArea', params)
  }
}
