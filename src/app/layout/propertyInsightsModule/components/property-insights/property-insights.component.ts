import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { faSquare, faCheckSquare } from '@fortawesome/free-solid-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ChartComponent } from 'ng-apexcharts';
import { PropertyInsightsService } from './property-insights.service';
import { UtilityService } from 'src/app/shared/utitlity.service';
import { Observable, of } from 'rxjs';
import { GridApi } from 'ag-grid-community';
import { NgbModal, NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { Pipe, PipeTransform } from '@angular/core';
import { faFilm } from '@fortawesome/free-solid-svg-icons';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { FormControl, FormGroup } from '@angular/forms';
am4core.useTheme(am4themes_animated);
import ApexCharts from 'apexcharts';

@Pipe({ name: 'round' })
export class RoundPipe implements PipeTransform {
  /**
   *
   * @param value
   * @returns {number}
   */
  transform(value: number): number {
    return Math.round(value);
  }
}

class ZooplaData {
  town_city: any;
  keywords: any;
  listening_status: any;
  maximum_price: any;
  minimum_price: any;
  orderBy: any;
  page_size: any;
  postcode: any;
  property_type: any;
  radius: any;
  summarised: any;
  number_of_beds: any;
}
class SearchData {
  build_type: any;
  district: any;
  bedrooms: any;
  postcode: any;
  prop_type: any;
  town_city: any;
  buildYear: any;
  currentCondition: any;
  numberOfBaths: any;
  numberOfGarage: any;
  typeOfGarden: any;
  receptions: any;
  internalarea: any;
  address: any;
  priceOrValuation: any;
  parking: any;
  // results: any = [];
}

@Component({
  selector: 'app-property-insights',
  templateUrl: './property-insights.component.html',
  styleUrls: ['./property-insights.component.scss']
})
export class PropertyInsightsComponent implements OnInit {

  private _searchFormData: SearchData;
  dataList: any;
  currentJustify = 'fill';
  currentRate = 2;
  latitude = 12.972442;
  longitude = 77.580643;
  mapType = 'satellite';
  result: any;
  value: any;
  zooplaList: any[] = [];
  rentValue: any;
  columnDefs: any;
  similarrowData$: Observable<any[]> = of([]);
  similarcolumnDefs: any;
  frameworkComponents;
  gridApi: GridApi;
  jsArray: any;
  list123: any[];
  inputTableBuiltYear: any;
  panelOpenState = false;
  cashFlowDatas: any;
  cashFlowTableData: any[] = [];
  filmIcon = faFilm;
  type = 'bar';
  pieseries1: any[] = [];
  @ViewChild('chartObj', null) chart: ChartComponent;
  @ViewChild('radarChart', null) radarChart: ChartComponent;
  @ViewChild('ngbTabset', null) ngbTabset: NgbTabset;
  private chart1: am4charts.RadarChart;
  constructor(private route: ActivatedRoute, private modalService: NgbModal, private round: RoundPipe,
    private service: PropertyInsightsService, private zone: NgZone) {
    library.add(faSquare, faCheckSquare);
    this.initGrid(true);
    // this.ngOnInit();
  }

  public doughnutChartLabels = ['Sales Q1', 'Sales Q2', 'Sales Q3', 'Sales Q4'];
  public doughnutChartData = [120, 150, 180, 90];
  public doughnutChartType = 'doughnut';

  public pieChartLabels = [' Q1', ' Q2', ' Q3', ' Q4'];
  public pieChartData = [11120, 43150, 142280, 2290];
  public pieChartType = 'pie';

  public barChartOptions: any = {
    responsive: true,
    scales: {
      yAxes: [
        {
          display: true,
          ticks: {
            beginAtZero: true
          }
        }
      ]
    },
    legend: {
      display: false
    }

  };
  objectKeys = Object.keys;
  rentalYield: any;
  annualRentalPayment: any;
  public barChartType: string = 'bar';
  public chartType: string = 'pie';
  public barChartLabels: string[] = [];
  barchartTitle = 'Yearly Cashflow';
  // barcolors = ['#36c6f4', '#00669b', '#777c77', '#2f3739'];
  public barChartData: any[] = [{
     data: [],
     label: 'CASH INVESTED',
      backgroundColor: ['#36c6f4', '#004766', '#00669b', '#777c77', '#2f3739', '#004766', '2f3739', '00669b', '#777c77', '#2f3739'] },
  ];
  oneOffCostsList = {
    downPayment: 'Down Payment',
    stampDuty: 'Stamp duty',
    renovationCost: 'Renovation Cost',
    mortgageAndValuationFees: 'Mortgage and Valuation Fees',
    legalPack: 'Legal Pack'
  };

  netMontlyList = {
    rentalPayments: 'Rental Payments',
    mortgagePayments: 'Mortgage Payment',
    miscExpenses: 'Misc Expenses',
    cleaningCost: 'Cleaning Cost'
  };

  annualcostList = {
    maintenanceCosts: 'Maintenance Costs',
    propertyManagement: 'Property Management',
    insurance: 'Insurance',
    voidPeriod: 'Void Period'
  };

  title = "value";
  public options: any = {
    chart: {
      height: 250,
      type: 'bar',
    },
    plotOptions: {
      bar: {
        horizontal: false,
        columnWidth: '60%'
      },
    },
    stroke: {
      show: true,
      width: 2,
    },
    dataLabels: {
      enabled: false
    },

    yaxis: {
      labels: {
        formatter: function (val) {
          return val / 1000 + 'K $'
        }
      }
    },
    fill: {
      opacity: 1,
    },
    xaxis: {
      type: 'datetime'
    }
  };

  // Apex radialbar chart
  radarseries = [];
  radarcolors = ['#36c6f4', '#00669b', '#777c77', '#2f3739', '#36c6f4', '#00669b', '#777c77', '#2f3739'];
  radarlabels = [];
  radarType = {
    type: 'radialBar'
  };
  plotOptions = {
    radialBar: {
      dataLabels: {
        name: {
          fontSize: '15px',
        },
        value: {
          fontSize: '13px',
        },
        total: {
          show: true,
          label: 'Total'
        }
      }
    }
  };
  // Apex radialbar chart ends

  // Apex pie chart
  radarTitle = '';
  pieChart = {
    type: 'pie',
    legend: {
      show: false,

    },
    width: 400,
    height: 400

  };
  pielabels = [];
  pieseries = [];
  piecolors = ['#36c6f4', '#00669b', '#777c77', '#004766', '#2f3739', '#087550', '#054766', 'blue'];
  responsive = [{
    breakpoint: 480,
    options: {
      chart: {
        width: 200
      },
      legend: {
        show: false,
        position: 'bottom',
        floating: false,
        horizontalAlign: 'center',
        ApexLegend: false
      }

    }
  }];
  // Apex pie chart ends


  public series: any = [{
    name: 'Yearly Profit',
    data: [{
      x: '2011',
      y: 1292
    }, {
      x: '2012',
      y: 4432
    }, {
      x: '2013',
      y: 5423
    }, {
      x: '2014',
      y: 6653
    }, {
      x: '2015',
      y: 8133,
      fillColor: '#00cbb9',
      strokeColor: '#0081cb'
    }, {
      x: '2016',
      y: 7132
    }, {
      x: '2017',
      y: 7332
    }, {
      x: '2018',
      y: 6553
    }]
  }];

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.result = params;
      this._searchFormData = JSON.parse(sessionStorage.getItem('searchFormData'));
      if (this._searchFormData.buildYear == 'pre') {
        this.inputTableBuiltYear = 'Pre 1914';
      } else if (this._searchFormData.buildYear == 'after') {
        this.inputTableBuiltYear = 'After 1914 - 2000';

      } else if (this._searchFormData.buildYear == 'new') {
        this.inputTableBuiltYear = 'After 2000';

      } else {
        this.inputTableBuiltYear = '';
      }

      const zooplaData = new ZooplaData();
      zooplaData.town_city = this._searchFormData.town_city;

      zooplaData.keywords = '';
      zooplaData.listening_status = '';
      zooplaData.maximum_price = '';
      zooplaData.minimum_price = '';
      zooplaData.orderBy = '';
      zooplaData.page_size = '';
      zooplaData.postcode = this._searchFormData.postcode;
      zooplaData.property_type = this._searchFormData.prop_type;
      zooplaData.radius = '';
      zooplaData.summarised = '';
      zooplaData.number_of_beds = this._searchFormData.bedrooms.toString();

      // similar properties
      this.service.getZoooplaEvaluation(zooplaData).subscribe(zoo => {
        this.zooplaList = zoo;
      });
      // rental valuation
      this.service.getRentalValuation(zooplaData).subscribe(val => {

        this.rentValue = val.rentalvaluation;
        this.rentalYield = this.round.transform(((this.rentValue * 12) / params.propertyInsightScore) * 100);
        let cashFlowInputValues = {
          propInsightValuation: params.propertyInsightScore,
          propRentalValuation: this.rentValue.toString(),
          propRentalYields: this.rentalYield.toString(),
        };
        let sumOfNetMonthly = 0;
        let sumOfAnnualCost = 0;
        let calculateCashFlow = 0;

        this.service.getCashFlowData(cashFlowInputValues).subscribe(data => {
          this.cashFlowDatas = data;
          for (let key of Object.keys(data)) {
            // radial chart from oneOffCosts
            for (let ofcosts of Object.keys(this.oneOffCostsList)) {
              if (key == ofcosts) {
                let ofcPer = this.round.transform(parseInt(this.cashFlowDatas[key], 10) / parseInt(this.cashFlowDatas.oneOffCosts, 10) * 100);
                this.radarseries.push(ofcPer);
                this.radarlabels.push(this.oneOffCostsList[key]);
              }
            }

            // pie chart from netmonthly and annualcost
            this.annualRentalPayment = this.round.transform(parseInt(this.cashFlowDatas.rentalPayments, 10) * 12);
            // net monthly calculation
            for (let netmonth of Object.keys(this.netMontlyList)) {
              if (key == netmonth && netmonth != 'rentalPayments') {
                let netMonPer = this.round.transform((((parseInt(this.cashFlowDatas[key], 10) * 12)) / this.annualRentalPayment) * 100);
                sumOfNetMonthly = sumOfNetMonthly + this.round.transform((parseInt(this.cashFlowDatas[key], 10) * 12));
                this.pieseries.push(netMonPer);
                this.pielabels.push(this.netMontlyList[key]);
              }
            }

            // annual costs calculation
            for (let acosts of Object.keys(this.annualcostList)) {
              if (key == acosts) {
                let annualCostPer = this.round.transform((parseInt(this.cashFlowDatas[key], 10) / this.annualRentalPayment) * 100);
                sumOfAnnualCost = sumOfAnnualCost + this.round.transform((parseInt(this.cashFlowDatas[key], 10)));
                this.pieseries1.push(annualCostPer);
                this.pielabels.push(this.annualcostList[key]);
              }

            }
          }
          const sumCF = sumOfNetMonthly + sumOfAnnualCost;
          calculateCashFlow = this.round.transform((this.annualRentalPayment - sumCF) * 100 / this.annualRentalPayment);
          this.pieseries = this.pieseries.concat(this.pieseries1);
          this.pieseries = this.pieseries.concat(calculateCashFlow);
          this.pielabels = this.pielabels.concat('Cash Flow');
          // bar chart
          const a = Object.values(data.cashFlowAnalysis);
          a.forEach((ele, i) => {
            const keys = Object.keys(ele);
            this.cashFlowTableData.push({ year: 'Year' + (i + 1), area: keys[0], value: ele[keys[0]] });
            this.barChartLabels.push('Year' + (i + 1));
          });
          this.cashFlowTableData.forEach(tableData => {
            this.barChartData.forEach(bChartData => {
              bChartData.data.push(tableData.value);
            });
          });
        });
      });
    });

  }

  initGrid(selectable: boolean) {

    this.columnDefs = [
      { headerName: 'Area', field: 'area', width: 300 },
      { headerName: 'Address', field: 'street', width: 300 },
      { headerName: 'Property Type', field: 'propertyType', width: 200 },
      { headerName: 'Beds', field: 'numberOfBeds', minwidth: 50 },
      { headerName: 'Baths', field: 'numberOfBaths', minwidth: 50 },
      { headerName: 'Asking Price', field: 'askingPrice', width: 200 },
      { headerName: 'Listed on', field: 'listedOn', width: 200 },
      { headerName: 'Area Sqm', field: 'areaSqm', },
      { headerName: 'Distance', field: 'distance', },
      { headerName: 'Price/Sqm', field: 'priceSqm', },

    ];

  }
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
    this.gridApi.setGridAutoHeight(true);
    this.gridApi.hideOverlay();
  }

}
