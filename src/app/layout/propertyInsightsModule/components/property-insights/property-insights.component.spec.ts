import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyInsightsComponent } from './property-insights.component';

describe('PropertyInsightsComponent', () => {
  let component: PropertyInsightsComponent;
  let fixture: ComponentFixture<PropertyInsightsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyInsightsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyInsightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
