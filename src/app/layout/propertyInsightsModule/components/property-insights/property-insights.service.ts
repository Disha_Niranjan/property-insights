import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ApplicationHttpClient } from 'src/app/shared/guard/ApplicationHttpClient.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { NgxXml2jsonService } from 'ngx-xml2json';

@Injectable()
export class PropertyInsightsService {

  constructor(private http: HttpClient, private httpc: ApplicationHttpClient, private ngxXml2jsonService: NgxXml2jsonService) { }

  getProperyEvaluation(params) {
    return this.httpc.Post('v1/property_evaluation', params);
  }

  getZoooplaEvaluation(params): Observable<any> {
    return this.httpc.Post('v1/zoopla/similarprops', params);
  }

  getRentalValuation(params): Observable<any> {
    return this.httpc.Post('v1/zoopla/rentalValuation', params);

  }

  getCashFlowData(params): Observable<any> {
    return this.httpc.Post('v1/cashflow_yields', params);
  }
}
