import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DealPropertyInsightsComponent } from './deal-property-insights.component';

describe('DealPropertyInsightsComponent', () => {
  let component: DealPropertyInsightsComponent;
  let fixture: ComponentFixture<DealPropertyInsightsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DealPropertyInsightsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DealPropertyInsightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
