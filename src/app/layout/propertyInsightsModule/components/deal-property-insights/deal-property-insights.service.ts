import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ApplicationHttpClient } from 'src/app/shared/guard/ApplicationHttpClient.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { NgxXml2jsonService } from 'ngx-xml2json';

@Injectable()
export class DealPropertyInsightsService {

    constructor(private http: HttpClient, private httpc: ApplicationHttpClient, private ngxXml2jsonService: NgxXml2jsonService) { }

    //   getAllExportOrdersByTypeAndStatus(type: string, status: string): Observable<any> {
    //     return this.http.Get('/getAllExportSalesOrdersByStatus/' + type + '/' + status);
    //   }

    getAllPropertyInsightsList() {

        console.log('enter into service');
        const url = 'http://api.zoopla.co.uk/api/v1/property_listings.xml?postcode=HA1&area=Harrow&api_key=thf3rcpfd85rzcnwn3mve586&radius=0.1&listing_status=sale&order_by=age&minimum_price=200000&maximum_price=800000&property_type=houses&keywords=auction&summarised=yes&page_size=5';

        return this.http.get(url,
            {responseType: 'text'});
    }

    getProperyEvaluation(params){
      debugger
        return this.httpc.Post('v1/property_evaluation' ,  params);
    }
 
}
