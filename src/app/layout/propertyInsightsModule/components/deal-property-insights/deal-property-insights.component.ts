import { Component, OnInit, ViewChild } from '@angular/core';
import { faSquare, faCheckSquare } from '@fortawesome/free-solid-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ChartComponent } from 'ng-apexcharts';
import { UtilityService } from 'src/app/shared/utitlity.service';
import { DealPropertyInsightsService } from './deal-property-insights.service';
import { CommonModalComponent } from 'src/app/shared/common-modal/common-modal.component';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { DealFinderService } from 'src/app/layout/dealFinderModule/components/deal-finder/deal-finder.service';
import { GridApi } from 'ag-grid-community';

class DealSearchData {
  build_type: any;
  numberhabitablerooms: any;
  postcode: any;
  prop_type: any;
  town_city: any;
  district: any;
}
@Component({
  selector: 'app-deal-property-insights',
  templateUrl: './deal-property-insights.component.html',
  styleUrls: ['./deal-property-insights.component.scss']
})
export class DealPropertyInsightsComponent implements OnInit {
  dataList: any;
  currentJustify = 'fill';
  currentRate = 2;

  latitude = 12.972442;
  longitude = 77.580643;
  mapType = 'satellite';


  result: any;
  value: any;
  isdealfinder: boolean;
  loading: boolean = false;
  selectedTab = 'tab-selectbyid1';
  similarList: any;
  searchdata;

  similarcolumnDefs: any;
  frameworkComponents;
  gridApi: GridApi;
  @ViewChild('chartObj', null) chart: ChartComponent;
  activeIdString;

  constructor(private route: ActivatedRoute,
    private dealservice: DealFinderService,
    private utilService: UtilityService, private modalService: NgbModal,
    private service: DealPropertyInsightsService) {
    library.add(faSquare, faCheckSquare);
    this.isdealfinder = this.utilService.comeFromDealFinder;
    console.log(this.isdealfinder);
    this.activeIdString = 'similar';
    this.initGrid(true);

  }

  public doughnutChartLabels = ['Sales Q1', 'Sales Q2', 'Sales Q3', 'Sales Q4'];
  public doughnutChartData = [120, 150, 180, 90];
  public doughnutChartType = 'doughnut';

  public pieChartLabels = [' Q1', ' Q2', ' Q3', ' Q4'];
  public pieChartData = [11120, 43150, 142280, 2290];
  public pieChartType = 'pie';

  public barChartOptions: any = {
    responsive: true,
    scales: {
      yAxes: [
        {
          display: true,
          ticks: {
            beginAtZero: true
          }
        }
      ]
    },
    legend: {
      display: false
    }

  };

  public barChartType: string = 'bar';
  public chartType: string = 'pie';
  public barChartLabels: string[] = ['MAY', 'JUL', 'SEP', 'NOV'];
  public barChartData: any[] = [{ data: [10, 50, 10, 15], label: 'All Jobs',/*  backgroundColor: '#f9d8d8', borderColor: '#f9d8d8' */ },
  { data: [0, 60, 20, 30], label: 'Completed Jobs'/* , backgroundColor: '#f3e1c3', borderColor: '#f9c870' */ },
  { data: [5, 30, 80, 12], label: 'In Process Jobs'/* , backgroundColor: '#fbfbbf', borderColor: '#fbfbbf'  */ },
  ];

  userData = [
    { userName: 'Ethan Smith', discription: 'Guys, check this out: I am not able to find any bug in this app', time: 'Today at 12:30pm' },
    { userName: 'Ethan Smith', discription: 'Guys, check this out: I am not able to find any bug in this app', time: 'Today at 12:30pm' },
    { userName: 'Ethan Smith', discription: 'Guys, check this out: I am not able to find any bug in this app', time: 'Today at 12:30pm' }
  ];


  title: "value";
  public options: any = {
    chart: {
      height: 250,
      type: 'bar',
    },
    plotOptions: {
      bar: {
        horizontal: false,
        columnWidth: '60%'
      },
    },
    stroke: {
      show: true,
      width: 2,
    },
    dataLabels: {
      enabled: false
    },

    yaxis: {
      labels: {
        formatter: function (val) {
          return val / 1000 + 'K $'
        }
      }
    },
    fill: {
      opacity: 1,
    },
    xaxis: {
      type: 'datetime'
    }
  };

  public series: any = [{
    name: 'Yearly Profit',
    data: [{
      x: '2011',
      y: 1292
    }, {
      x: '2012',
      y: 4432
    }, {
      x: '2013',
      y: 5423
    }, {
      x: '2014',
      y: 6653
    }, {
      x: '2015',
      y: 8133,
      fillColor: '#00cbb9',
      strokeColor: '#0081cb'
    }, {
      x: '2016',
      y: 7132
    }, {
      x: '2017',
      y: 7332
    }, {
      x: '2018',
      y: 6553
    }]
  }]

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.result = params;
      this.searchdata = JSON.parse(sessionStorage.getItem('DealSearchFormData'));
      // this.service.getAllDealFinderList().subscribe(res => {
      this.dealservice.getDealSearchList(this.searchdata).subscribe(result => {
        this.similarList = result;
      })
      console.log(this.similarList);



    });
  }

  selectTabView(event) {
    this.activeIdString = event.nextId;
  }

  
  initGrid(selectable: boolean) {

    this.similarcolumnDefs = [
      { headerName: 'Address', field: 'address', width: 380 },
      { headerName: 'Town', field: 'postTown', },
      { headerName: 'Asking Price', field: 'absolutePrice', },

      { headerName: 'Property Type', field: 'propertyType', },
      { headerName: 'Beds', field: 'numOfBedrooms', width: 130 },
      { headerName: 'Baths', field: 'numOfBath', width: 130},

    ];

  }
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridApi.sizeColumnsToFit();
    this.gridApi.setGridAutoHeight(true);
    this.gridApi.hideOverlay();
  }

}
