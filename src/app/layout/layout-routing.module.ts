import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { HeaderComponent } from './components/header/header.component';
// import { AuthGuard } from '../shared/guard/auth.guard';

const routes: Routes = [
    {
        path: 'pages',
        component: LayoutComponent,
        // canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'login', loadChildren: () => import('../layout/login/login.module').then(m => m.LoginModule) },
            { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule) },
            { path: 'header', component: HeaderComponent },
            { path: 'propertyAnalyzer', loadChildren: () => import ('./property-analyzer/property-analyzer.module').then(m => m.PropertyAnalyzerModule) },
            { path: 'propertyDeals', loadChildren: () => import ('./property-deals/property-deals.module').then(m => m.PropertyDealsModule) },
            { path: 'dealFinderModule', loadChildren: () => import ('./dealFinderModule/dealFinderModule.module').then(m => m.DealFinderModule) },
            { path: 'propertyInsightsModule', loadChildren: () => import ('./propertyInsightsModule/propertyInsightsModule.module').then(m => m.PropertyInsightsModule) },


        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
