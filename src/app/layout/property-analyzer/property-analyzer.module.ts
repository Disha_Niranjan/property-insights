import { CommonModule } from '@angular/common';
import { PropertyAnalyzer} from './property-analyzer.component';
import { PropertyDetailsComponent } from './property-details/property-details.component';
import { routing } from './property-analyzer.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from 'src/app/shared/shared.module';
import { ChartsModule as Ng2Charts, ChartsModule } from 'ng2-charts';

@NgModule({
  imports: [
    CommonModule,
    routing, 
    ReactiveFormsModule,
    FormsModule,
    NgbDropdownModule,
    NgbModalModule,
    SharedModule,
    NgbModule,
    ChartsModule

  ],
  declarations: [
    PropertyAnalyzer,
    PropertyDetailsComponent,
  ],
  entryComponents: [
    
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class PropertyAnalyzerModule { }
