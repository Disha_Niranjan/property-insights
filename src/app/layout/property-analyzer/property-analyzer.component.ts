import {Component} from '@angular/core';

@Component({
  selector: 'propertyAnalyzer',
  template: `<router-outlet></router-outlet>`
})
export class PropertyAnalyzer {
  constructor() {
  }
}
