import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../../../router.animations';
@Component({
  selector: 'app-property-details',
  templateUrl: './property-details.component.html',
  styleUrls: ['./property-details.component.scss']
})
export class PropertyDetailsComponent implements OnInit {

  constructor(
     private router: Router,
    private route: ActivatedRoute) {
    console.log('changing page')
    // let subuser = localStorage.getItem('user')

    //  if( subuser != 'ADMIN'){
    //   this.router.navigate(['pages/dashboard'])
    // }
  }

    
    public barChartOptions: any = {
      responsive: true,
      scales: {
        yAxes: [
          {
            display: true,
            ticks: {
              beginAtZero: true
            }
          }
        ]
      },
      legend: {
        display: false
      }
  
    };
  
  
    public barChartType: string = 'line';
    public barChartLabels: string[] = ['MAY', 'JUL', 'SEP', 'NOV'];
    public barChartData: any[] = [{ data: [10, 50, 10, 15], label: 'All Jobs',/*  backgroundColor: '#f9d8d8', borderColor: '#f9d8d8' */ },
    { data: [0, 60, 20, 30], label: 'Completed Jobs'/* , backgroundColor: '#f3e1c3', borderColor: '#f9c870' */ },
    { data: [5, 30, 80, 12], label: 'In Process Jobs'/* , backgroundColor: '#fbfbbf', borderColor: '#fbfbbf'  */},
    ];
  
    userData = [
      { userName: 'Ethan Smith', discription: 'Guys, check this out: I am not able to find any bug in this app', time: 'Today at 12:30pm' },
      { userName: 'Ethan Smith', discription: 'Guys, check this out: I am not able to find any bug in this app', time: 'Today at 12:30pm' },
      { userName: 'Ethan Smith', discription: 'Guys, check this out: I am not able to find any bug in this app', time: 'Today at 12:30pm' }
    ];
  
  ngOnInit() {
    
  }

}
