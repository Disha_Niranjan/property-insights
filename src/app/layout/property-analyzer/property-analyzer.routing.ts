import { Routes, RouterModule } from '@angular/router';
import { PropertyAnalyzer} from './property-analyzer.component';
import { PropertyDetailsComponent } from './property-details/property-details.component';

const routes: Routes = [
  {
    path: '',
    component: PropertyAnalyzer,
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'propertyDetails', component: PropertyDetailsComponent },
    ]
  }
];

export const routing = RouterModule.forChild(routes);
