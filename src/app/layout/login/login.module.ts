import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { WordPressLoginComponent } from './wordPressLogin/wordPressLogin.component';
import { WordPressSignupComponent } from './wordPressSignup/wordPressSignup.component';
import { MatCheckboxModule } from '@angular/material';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        TranslateModule,
        LoginRoutingModule,
        MatCheckboxModule,
        FormsModule
    ],
    declarations: [
        LoginComponent,
        WordPressLoginComponent,
        WordPressSignupComponent
    ],
    entryComponents: [],
})
export class LoginModule {}
