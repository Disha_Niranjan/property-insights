import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../../../router.animations';
// import { AuthService } from '../shared';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { NgbModal, NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CommonModalComponent } from 'src/app/shared/common-modal/common-modal.component';

@Component({
  selector: 'add-service-modal',
  styleUrls: ['./roles-modal.component.scss'],
  templateUrl: './roles-modal.component.html',
  animations: [routerTransition()]

})

export class RolesModalComponent implements OnInit {

  modalHeader: string;

  showHide: boolean;

  modalContent: string;

  oKMessage: string = 'Roles';

  cancelMessage: string = 'Cancel';
  error: string = null;
  isLoading = false;

  constructor(private activeModal: NgbActiveModal, private router: Router,
    ) { }

  ngOnInit() { }


  cancelModal() {
    this.activeModal.close('N');
  }

  okModal() {
    this.activeModal.close('Y');
  }

  roles() {
      this.router.navigate(['pages/propertyAnalyzer/propertyDetails']);
      this.cancelModal();
  }






}


