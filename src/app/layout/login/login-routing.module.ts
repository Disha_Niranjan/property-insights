import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';
import { WordPressLoginComponent } from './wordPressLogin/wordPressLogin.component';
import { WordPressSignupComponent } from './wordPressSignup/wordPressSignup.component';

const routes: Routes = [
    {
        path: '',
        component: LoginComponent
    },
    { path: 'wplogin', component: WordPressLoginComponent },
    { path: 'wpsignup', component: WordPressSignupComponent },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LoginRoutingModule { }
