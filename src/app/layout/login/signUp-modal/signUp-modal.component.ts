import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../../../router.animations';
// import { AuthService } from '../shared';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { NgbModal, NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CommonModalComponent } from 'src/app/shared/common-modal/common-modal.component';
import { RolesModalComponent } from '../roles-modal/roles-modal.component';
import { AuthService } from '../../../shared/guard/auth.service';
@Component({
  selector: 'add-service-modal',
  styleUrls: ['./signUp-modal.component.scss'],
  templateUrl: './signUp-modal.component.html',
  animations: [routerTransition()]

})

export class SignUpModalComponent implements OnInit {
  checked = false;
  signupForm: FormGroup;
  emailMessage: boolean = false;
  status: string;
  validUser: boolean = false;
  usernameinvalid: boolean = false;
  emailinvalid: boolean = false;
  passwordinvalid: boolean = false;

  public username: AbstractControl;
  public address: AbstractControl;
  public phoneNumber: AbstractControl;
  public email: AbstractControl;
  public fullname: AbstractControl;
  public password: AbstractControl;

  constructor(private activeModal: NgbActiveModal,
    private modalService: NgbModal,
    private authService: AuthService,
    private router: Router, private fb: FormBuilder,
    private route: ActivatedRoute) {
    this.createForm();
  }

  ngOnInit() {

  }

  showAlert(flag: boolean): NgbModalRef {
    const activeModal = this.modalService.open(CommonModalComponent, { size: 'sm' });
    activeModal.componentInstance.showHide = flag;
    activeModal.componentInstance.modalHeader = 'Message';
    activeModal.componentInstance.oKMessage = 'Ok';
    return activeModal;
  }
  // onMessage(msg: string) {
  //   this.emailMessage = msg;
  //   this.email.setValue('');
  //   setTimeout(() => this.emailMessage = null, 10000);
  //   this.status = 'warning';
  // } .
  signup() {
    if (this.email.valid && this.username.valid) {
      this.authService.checkEmail(this.email.value).subscribe(res => {
        if (res.details == 100) {
          this.emailMessage = true;
        } else {
          this.emailMessage = false;
        }
        if (this.username.valid) {
          this.authService.checkUsername(this.username.value).subscribe(user => {

            if (user.details == 100) {
              this.validUser = true;
            } else {
              this.validUser = false;
            }
            if (!this.validUser && !this.emailMessage) {
              this.authService.getSignup(this.signupForm.value).subscribe(results => {
                if (results.details == 100) {
                  const activeModal = this.showAlert(false);
                  activeModal.componentInstance.modalContent = results.message;
                }
                if (results.details == 200) {
                  this.activeModal.close();
                  const activeModal = this.showAlert(false);
                  activeModal.componentInstance.modalContent = results.message;
                  // this.authService.login(details)
                }
              });
            }
          });
        }

      });
    } else {
      if (this.email.invalid) {
        this.usernameinvalid = true;
      }
      if (this.username.invalid) {
        this.emailinvalid = true;
      }
      if (this.password.invalid) {
        this.passwordinvalid = true;
      }
    }
  }

  cancelModal() {
    this.activeModal.close(false);
  }

  private createForm() {
    this.signupForm = this.fb.group({
      'username': ['', Validators.required],
      'address': [''],
      'phoneNumber': ['', Validators.required],
      'email': ['', Validators.required],
      'fullname': ['', Validators.required],
      'password': ['', Validators.required],

    });

    this.username = this.signupForm.controls['username'];
    this.address = this.signupForm.controls['address'];
    this.phoneNumber = this.signupForm.controls['phoneNumber'];
    this.email = this.signupForm.controls['email'];
    this.fullname = this.signupForm.controls['fullname'];
    this.password = this.signupForm.controls['password'];

  }

}


