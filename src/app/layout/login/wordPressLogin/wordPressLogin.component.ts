import { ActivatedRoute, Params, Router, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { UtilityService } from 'src/app/shared/utitlity.service';
import { Component, OnInit, EventEmitter } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { NgbModal, NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CommonModalComponent } from 'src/app/shared/common-modal/common-modal.component';
import { RolesModalComponent } from '../roles-modal/roles-modal.component';
import { AuthService } from '../../../shared/guard/auth.service';
import { SignUpModalComponent } from '../signUp-modal/signUp-modal.component';

@Component({
  selector: 'app-wordPressLogin',
  templateUrl: './wordPressLogin.component.html',
  styleUrls: ['./wordPressLogin.component.scss']
})

export class WordPressLoginComponent implements OnInit {

  error: string = null;
  isLoading = false;
  loginForm: FormGroup;
  isError: boolean = false;

  public username: AbstractControl;
  public password: AbstractControl;

  constructor(private route: ActivatedRoute,
    private location: Location, private modalService: NgbModal,
    private fb: FormBuilder,
    private router: Router,
    private utilService: UtilityService, private authService: AuthService,
  ) { this.createForm(); }

  ngOnInit() { }

  login() {
    this.isLoading = true;
    this.authService.login(this.loginForm.value).subscribe(credentials => {
      sessionStorage.setItem('loggedUser', credentials.username);
      this.authService.loadUserRole().subscribe(role => {
      });
      this.authService.loggedIn.next(true);
      this.utilService.isUserLogin = true;
      this.router.navigate(['pages/dashboard']);
    }, error => {
      this.isError = true;
      this.finally();
    });

  }

  finally() {
    this.isLoading = false;
    this.loginForm.markAsPristine();
  }

  signUp() {
    this.router.navigate(['pages/login/wpsignup']);
  }

  private createForm() {
    this.loginForm = this.fb.group({
      'username': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
    });

    this.username = this.loginForm.controls['username'];
    this.password = this.loginForm.controls['password'];
  }

}
