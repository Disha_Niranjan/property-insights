import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WordPressLoginComponent } from './wordPressLogin.component';

describe('WordPressLoginComponent', () => {
  let component: WordPressLoginComponent;
  let fixture: ComponentFixture<WordPressLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WordPressLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WordPressLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
