import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../../../router.animations';
// import { AuthService } from '../shared';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { NgbModal, NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { CommonModalComponent } from 'src/app/shared/common-modal/common-modal.component';
import { RolesModalComponent } from '../roles-modal/roles-modal.component';
import { AuthService } from '../../../shared/guard/auth.service';
import { UtilityService } from 'src/app/shared/utitlity.service';
import { SignUpModalComponent } from '../signUp-modal/signUp-modal.component';
@Component({
  selector: 'add-login-modal',
  styleUrls: ['./login-modal.component.scss'],
  templateUrl: './login-modal.component.html',
  animations: [routerTransition()]

})

export class LoginModalComponent implements OnInit {

  modalHeader: string;

  showHide: boolean;

  modalContent: string;

  oKMessage: string = 'Login';

  cancelMessage: string = 'Cancel';
  error: string = null;
  isLoading = false;
  loginForm: FormGroup;
  ferror: boolean = false;
  isError: boolean = false;

  public username: AbstractControl;
  public password: AbstractControl;
  public group: AbstractControl;

  constructor(private activeModal: NgbActiveModal,
    private modalService: NgbModal, private authService: AuthService, private router: Router, public utilService: UtilityService,
    private fb: FormBuilder, private route: ActivatedRoute) { this.createForm(); }

  ngOnInit() { }


  cancelModal() {
    this.activeModal.close(false);
  }
  login() {
    this.isLoading = true;

    if (this.username.valid && this.password.valid) {
      this.authService.login(this.loginForm.value).subscribe(credentials => {
        sessionStorage.setItem('loggedUser', credentials.username);
        this.authService.loadUserRole().subscribe(role => {
          // this.router.navigate(['/']);
          // this.isLogin.emit(true);
          // this.isLogin.next(true);
          this.activeModal.close(true);
        });
        this.authService.loggedIn.next(true);
        this.utilService.isUserLogin = true;
        this.ferror = false;
        this.router.navigate(['pages/dashboard']);
      }, error => {
        // this.activeModal.close(false);
        this.isError = true;
        this.finally();
      });
    } else {
      this.ferror = true;
    }
  }

  showAlert(flag: boolean): NgbModalRef {
    const activeModal = this.modalService.open(RolesModalComponent, { size: 'lg' });
    activeModal.componentInstance.showHide = flag;
    activeModal.componentInstance.modalHeader = 'Roles';
    return activeModal;
  }

  finally() {
    this.isLoading = false;
    this.loginForm.markAsPristine();
  }

  forgot_password() {
    this.router.navigate(['login/forget-password']);
  }

  signUp() {
    this.activeModal.close();
    const activeModal = this.modalService.open(SignUpModalComponent, { size: 'sm' });
    activeModal.componentInstance.showHide = true;
    return activeModal;
  }

  private createForm() {
    this.loginForm = this.fb.group({
      'username': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
    });

    this.username = this.loginForm.controls['username'];
    this.password = this.loginForm.controls['password'];
  }

}
