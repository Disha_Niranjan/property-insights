import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WordPressSignupComponent } from './wordPressSignup.component';

describe('WordPressSignupComponent', () => {
  let component: WordPressSignupComponent;
  let fixture: ComponentFixture<WordPressSignupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WordPressSignupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WordPressSignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
