import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { LanguageTranslationModule } from './shared/modules/language-translation/language-translation.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard, AuthService } from './shared/guard';
import { TokenInterceptor } from './shared/guard/token.interceptor';
import { ApplicationHttpClient, applicationHttpClientCreator } from './shared/guard/ApplicationHttpClient.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faSquare, faCheckSquare } from '@fortawesome/free-solid-svg-icons';
import { AgGridModule } from 'ag-grid-angular/main';
import { UserManagementComponent } from './user-management/user-management.component';
import { SharedModule } from './shared/shared.module';
import { LayoutModule } from './layout/layout.module'
import { CommonModalComponent } from './shared/common-modal/common-modal.component';
import { enableProdMode } from '@angular/core';
import { GestureConfig } from '@angular/material';
import { MaterialModule } from './shared/material-module';
import { AgmCoreModule } from '@agm/core';
import { UtilityService } from './shared/utitlity.service';
import { LoginModalComponent } from './layout/login/login-modal/login-modal.component';
import { StorageServiceModule } from 'angular-webstorage-service';
import { SignUpModalComponent } from './layout/login/signUp-modal/signUp-modal.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    // LanguageTranslationModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    AgGridModule.withComponents([]),
    SharedModule,
    LayoutModule,
    MaterialModule,
    CommonModule,
    StorageServiceModule,
    AgmCoreModule.forRoot({}),
    // AgmCoreModule.forRoot({apiKey: 'AIzaSyD1bxd6Lei2nWTP31tuZctMmqxnZ0log7Y'})
  ],
  declarations: [
    AppComponent,
    LoginModalComponent,
    SignUpModalComponent,
    UserManagementComponent
  ],
  providers: [UtilityService,
    AuthGuard, AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: ApplicationHttpClient,
      useFactory: applicationHttpClientCreator,
      deps: [HttpClient]
    },
    { provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    CommonModalComponent,
    LoginModalComponent,
    SignUpModalComponent
  ]
})
export class AppModule {
  constructor() {
    library.add(faSquare, faCheckSquare);
  }
}
